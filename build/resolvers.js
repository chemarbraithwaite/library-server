"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _mongodb = require("mongodb");

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var LIBRARY = 'library';
var BRANCH_ID = 'branchId';
var resolvers = {
  Query: {
    libraries: function () {
      var _libraries = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_, __, context) {
        var libraries;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return context.dbClient.collection(LIBRARY).find({}).toArray();

              case 2:
                libraries = _context.sent;
                return _context.abrupt("return", libraries.map(function (library) {
                  delete library.books;
                  library.id = library._id;
                  delete library._id;
                  return library;
                }));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function libraries(_x, _x2, _x3) {
        return _libraries.apply(this, arguments);
      }

      return libraries;
    }(),
    books: function () {
      var _books = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(_, __, context) {
        var libraries, books, _iterator, _step, _loop;

        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return context.dbClient.collection(LIBRARY).find({}).toArray();

              case 2:
                libraries = _context2.sent;
                books = [];
                _iterator = _createForOfIteratorHelper(libraries);

                try {
                  _loop = function _loop() {
                    var library = _step.value;
                    var libraryBooks = library.books.map(function (book) {
                      book.branchId = library._id;
                      return book;
                    });
                    books = books.concat(libraryBooks);
                  };

                  for (_iterator.s(); !(_step = _iterator.n()).done;) {
                    _loop();
                  }
                } catch (err) {
                  _iterator.e(err);
                } finally {
                  _iterator.f();
                }

                return _context2.abrupt("return", books);

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function books(_x4, _x5, _x6) {
        return _books.apply(this, arguments);
      }

      return books;
    }(),
    getLibraryBooks: function () {
      var _getLibraryBooks = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(_, _ref, context) {
        var libraryId, library;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                libraryId = _ref.libraryId;
                _context3.next = 3;
                return context.dbClient.collection(LIBRARY).findOne({
                  _id: new _mongodb.ObjectId(libraryId)
                });

              case 3:
                library = _context3.sent;

                if (library) {
                  _context3.next = 6;
                  break;
                }

                throw Error("Cannot find library with id ".concat(libraryId));

              case 6:
                return _context3.abrupt("return", library.books.map(function (book) {
                  book.branchId = library._id;
                  return book;
                }));

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getLibraryBooks(_x7, _x8, _x9) {
        return _getLibraryBooks.apply(this, arguments);
      }

      return getLibraryBooks;
    }(),
    getLibrary: function () {
      var _getLibrary = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(_, _ref2, context) {
        var id, library;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = _ref2.id;
                _context4.next = 3;
                return context.dbClient.collection(LIBRARY).findOne({
                  _id: new _mongodb.ObjectId(id)
                });

              case 3:
                library = _context4.sent;

                if (library) {
                  _context4.next = 6;
                  break;
                }

                throw Error("Cannot find library with id ".concat(id));

              case 6:
                return _context4.abrupt("return", {
                  id: library._id,
                  branch: library.branch,
                  books: library.books.map(function (book) {
                    book[BRANCH_ID] = library._id;
                    return book;
                  })
                });

              case 7:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getLibrary(_x10, _x11, _x12) {
        return _getLibrary.apply(this, arguments);
      }

      return getLibrary;
    }(),
    authors: function () {
      var _authors = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(_, __, context) {
        var libraries, authors, authorsArray;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return context.dbClient.collection(LIBRARY).find({}).toArray();

              case 2:
                libraries = _context5.sent;
                authors = new Set();
                libraries.forEach(function (library) {
                  library.books.forEach(function (book) {
                    authors.add(book.author);
                  });
                });
                authorsArray = Array.from(authors);
                return _context5.abrupt("return", authorsArray.map(function (author) {
                  return {
                    name: author
                  };
                }));

              case 7:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function authors(_x13, _x14, _x15) {
        return _authors.apply(this, arguments);
      }

      return authors;
    }()
  },
  Mutation: {
    createBook: function () {
      var _createBook = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(_, args, context) {
        var _args$input, title, author, year, genre, branchId, newBook, response;

        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _args$input = args.input, title = _args$input.title, author = _args$input.author, year = _args$input.year, genre = _args$input.genre, branchId = _args$input.branchId;
                newBook = {
                  id: new _mongodb.ObjectId(),
                  year: year,
                  title: title,
                  author: author.name,
                  genre: genre !== null && genre !== void 0 ? genre : []
                };
                _context6.next = 4;
                return context.dbClient.collection(LIBRARY).updateOne({
                  _id: new _mongodb.ObjectId(branchId)
                }, {
                  $push: {
                    "books": newBook
                  }
                }, {
                  upsert: false
                });

              case 4:
                response = _context6.sent;

                if (!(response.result.nModified !== 1)) {
                  _context6.next = 7;
                  break;
                }

                throw Error('Error creating new book');

              case 7:
                newBook.branchId = branchId;
                return _context6.abrupt("return", newBook);

              case 9:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function createBook(_x16, _x17, _x18) {
        return _createBook.apply(this, arguments);
      }

      return createBook;
    }(),
    createLibrary: function () {
      var _createLibrary = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(_, _ref3, context) {
        var branch, newLibrary, response;
        return _regenerator["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                branch = _ref3.branch;
                newLibrary = {
                  _id: new _mongodb.ObjectId(),
                  branch: branch,
                  books: []
                };
                _context7.next = 4;
                return context.dbClient.collection(LIBRARY).insertOne(newLibrary);

              case 4:
                response = _context7.sent;

                if (!(response.result.ok !== 1)) {
                  _context7.next = 7;
                  break;
                }

                throw Error('Error creating new library');

              case 7:
                newLibrary.id = newLibrary._id;
                delete newLibrary._id;
                return _context7.abrupt("return", newLibrary);

              case 10:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));

      function createLibrary(_x19, _x20, _x21) {
        return _createLibrary.apply(this, arguments);
      }

      return createLibrary;
    }(),
    deleteLibrary: function () {
      var _deleteLibrary = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(_, _ref4, context) {
        var branchId, response;
        return _regenerator["default"].wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                branchId = _ref4.branchId;
                _context8.next = 3;
                return context.dbClient.collection(LIBRARY).deleteOne({
                  _id: new _mongodb.ObjectId(branchId)
                });

              case 3:
                response = _context8.sent;

                if (!(!response.deletedCount || response.deletedCount === 0)) {
                  _context8.next = 6;
                  break;
                }

                throw Error("Error deleting library with Id ".concat(branchId));

              case 6:
                return _context8.abrupt("return", true);

              case 7:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));

      function deleteLibrary(_x22, _x23, _x24) {
        return _deleteLibrary.apply(this, arguments);
      }

      return deleteLibrary;
    }(),
    deleteBook: function () {
      var _deleteBook = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(_, _ref5, context) {
        var bookId, branchId, response;
        return _regenerator["default"].wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                bookId = _ref5.bookId, branchId = _ref5.branchId;
                _context9.next = 3;
                return context.dbClient.collection(LIBRARY).updateOne({
                  _id: new _mongodb.ObjectId(branchId)
                }, {
                  $pull: {
                    "books": {
                      "id": new _mongodb.ObjectId(bookId)
                    }
                  }
                }, {
                  upsert: false
                });

              case 3:
                response = _context9.sent;

                if (!(response.result.nModified === 0)) {
                  _context9.next = 6;
                  break;
                }

                throw Error("Error deleting book with Id ".concat(bookId));

              case 6:
                return _context9.abrupt("return", true);

              case 7:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }));

      function deleteBook(_x25, _x26, _x27) {
        return _deleteBook.apply(this, arguments);
      }

      return deleteBook;
    }()
  },
  Book: {
    author: function author(parent) {
      return {
        name: parent.author
      };
    }
  }
};
var _default = resolvers;
exports["default"] = _default;