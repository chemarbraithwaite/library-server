[![pipeline status](https://gitlab.com/chemarbraithwaite/library-server/badges/master/pipeline.svg)](https://gitlab.com/chemarbraithwaite/library-server/-/commits/master) [![coverage report](https://gitlab.com/chemarbraithwaite/library-server/badges/master/coverage.svg)](https://gitlab.com/chemarbraithwaite/library-server/-/commits/master)

# Library Server

A very simple application, developed to practice using [Apollo Server](https://www.apollographql.com/docs/apollo-server/) for implementing a GraphQL API.

The project uses [MongoDB](https://www.mongodb.com/) as the data store.

# Running project locally

1. Run `npm install` to install dependencies
2. The project uses a local instance of [MongoDB](https://www.mongodb.com/). Following the instructions [here](https://docs.mongodb.com/manual/installation/) to install and run MongoDB in your local environment.
3. Run `npm run start` to start the server
