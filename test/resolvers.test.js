const { ObjectId, MongoClient } = require("mongodb");
const { default: resolvers } = require("../resolvers");
const LIBRARY_COLLECTION = 'library';

const invaliId = 'anyInvalidId';
const branchId = 'branchId';
const defaultLibrary = [
    {
        "_id": ObjectId("604b08d8313ea78820db5b66"),
        "branch": "Liguanea",
        "books": [
            {
                "id": ObjectId("604b0afb313ea78820db5b69"),
                "year": 1948,
                "title": "A Better You",
                "author": "James Blake",
                "genre": [
                    "self-help"
                ]
            },
            {
                "id": ObjectId("604b0afb313ea78820db5b63"),
                "year": 2004,
                "title": "Eat Pray Love",
                "author": "Tamara Jones",
                "genre": [
                    "romance",
                    "drama"
                ]
            }
        ]
    },
    {
        "_id": ObjectId("604b08f1313ea78820db5b68"),
        "branch": "New Kingston",
        "books": [
            {
                "id": ObjectId("604b0afb313ea78820db5b39"),
                "year": 1994,
                "title": "Through The Rain",
                "author": "Leslie Williams",
                "genre": [
                    "comedy"
                ]
            },
            {
                "id": ObjectId("604b0afb313ea78820db5b63"),
                "year": 2004,
                "title": "Eat Pray Love",
                "author": "Tamara Jones",
                "genre": [
                    "romance",
                    "drama"
                ]
            }
        ]
    }
];

const  setUpDatabase = async () => {
    const connection = await MongoClient.connect(process.env.MONGO_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    const db = connection.db();
    const collections = await db.collections();

    if (collections.some(collection => collection.collectionName === LIBRARY_COLLECTION)) {
        db.collection(LIBRARY_COLLECTION).drop();
    }

    const context = {
        dbClient: db
    };
    return { connection, db, context };
}



describe('Query', () => {
    let connection;
    let db;
    let context;

    beforeAll(async () => {
        connection = await MongoClient.connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        db = connection.db();
        await db.collection(LIBRARY_COLLECTION).insertMany(defaultLibrary);
        context = {
            dbClient: db
        };
    });

    afterAll(async () => {
        await connection.close();
    });

    describe('libraries', () => {

        it('method returns the list of libraries in the db', async () => {
            const libraries = await resolvers.Query.libraries(null, null, context);

            const libraryObjects = defaultLibrary.map(library => { return { id: library._id, branch: library.branch } });

            expect(libraries).toEqual(libraryObjects);
        });
        it('if exception is thrown method throws', async () => {
            await expect(resolvers.Query.libraries(null, null, null)).rejects.toThrow('');
        });


    });

    describe('books', () => {

        it('method returns the list of library books in the db', async () => {
            const books = await resolvers.Query.books(null, null, context);

            const libraryObjects = defaultLibrary.reduce((booksList, currentLibrary) => {
                currentLibrary.books.forEach((_, index) => {
                    currentLibrary.books[index]['branchId'] = currentLibrary._id;
                });
                return booksList.concat(currentLibrary.books);
            }, []);

            expect(books).toEqual(libraryObjects);
        });

        it('if exception is thrown method throws', async () => {
            await expect(resolvers.Query.books(null, null, null)).rejects.toThrow();
        });


    });

    describe('getLibraryBooks', () => {

        it('method returns the list of books in a library in the db', async () => {
            const books = await resolvers.Query.getLibraryBooks(null, { libraryId: defaultLibrary[0]._id }, context);

            const libraryObjects = defaultLibrary[0].books.map((book) => {
                book[branchId] = defaultLibrary[0]._id;
                return book;
            });

            expect(books).toEqual(libraryObjects);
        });

        it('method throws if it cannot find library', async () => {
            await expect(resolvers.Query.getLibraryBooks(null, { libraryId: invaliId }, context)).rejects.toThrow(`Cannot find library with id ${invaliId}`);
        });



        it('if exception is thrown method throws', async () => {
            await expect(resolvers.Query.getLibraryBooks(null, { libraryId: null }, null)).rejects.toThrow();
        });


    });


    describe('getLibrary', () => {
        it('method returns the library with matching ID in the db', async () => {
            const library = await resolvers.Query.getLibrary(null, { id: defaultLibrary[0]._id }, context);

            const expectedLibrary = {
                id: defaultLibrary[0]._id,
                branch: defaultLibrary[0].branch,
                books: defaultLibrary[0].books.map(book => {
                    book[branchId] = defaultLibrary[0]._id;
                    return book;
                })
            }

            expect(library).toEqual(expectedLibrary);
        });

        it('method throws if it cannot find library', async () => {
            await expect(resolvers.Query.getLibrary(null, { id: invaliId }, context)).rejects.toThrow(`Cannot find library with id ${invaliId}`);
        });



        it('if exception is thrown method throws', async () => {
            await expect(resolvers.Query.getLibrary(null, { libraryId: null }, null)).rejects.toThrow();
        });


    });

    describe('authors', () => {
        it('method returns all unique authors in the db', async () => {
            const library = await resolvers.Query.authors(null, null, context);

            const expectedAuthors = defaultLibrary.reduce((previousBooks, currentLibrary) => {
                return previousBooks.concat(
                    currentLibrary.books.map(book => { return { name: book.author } }).filter(currentBook => !previousBooks.some(previousBook => previousBook.name === currentBook.name))
                );
            }, []);

            expect(library).toEqual(expectedAuthors);
        });



        it('if exception is thrown method throws', async () => {
            await expect(resolvers.Query.authors(null, null, null)).rejects.toThrow();
        });


    });

});

describe('Mutation', () => {
    describe('createBook', () => {
        let connection;
        let db;
        let context;

        beforeAll(async () => {
            ({connection, context, db} = await setUpDatabase())
        });

        afterAll(async () => {
            await connection.close();
        });

        it('method throws if library does not exist in db', async () => {
            const libraryId = '604b08d8313ea78820db5b66';
            const bookDetails = {
                "year": 1948,
                "title": "A Better You",
                "author": "James Blake",
                "genre": [
                    "self-help"
                ]
            };

            await expect(resolvers.Mutation.createBook(null, { input: { branchId: libraryId, genre: bookDetails.genre, year: bookDetails.year, title: bookDetails.title, author: { name: bookDetails.author } } }, context)).rejects.toThrow('Error creating new book');
        });

        it('book is inserted if library exists in db', async () => {
            const libraryId = '604b08d8313ea78820db5b67';
            const libraryDetails = {
                _id: ObjectId(libraryId),
                "branch": "New Kingston",
                "books": []
            };

            const bookDetails = {
                "year": 1948,
                "title": "A Better You",
                "author": "James Blake",
                "genre": [
                    "self-help"
                ]
            };

            await db.collection(LIBRARY_COLLECTION).insertOne(libraryDetails);
            await resolvers.Mutation.createBook(null, { input: { branchId: libraryId, genre: bookDetails.genre, year: bookDetails.year, title: bookDetails.title, author: { name: bookDetails.author } } }, context);
            const storedLibrary = await db.collection(LIBRARY_COLLECTION).findOne({ _id: ObjectId(libraryId) });
            const expectedBook = storedLibrary.books[0];
            delete expectedBook.id;
            expect(bookDetails).toEqual(expectedBook);

        });

        it('returned book matches inserted book', async () => {
            const libraryId = '604b08d8313ea78820db5b68';
            const libraryDetails = {
                _id: ObjectId(libraryId),
                "branch": "New Kingston",
                "books": []
            };

            const bookDetails = {
                "year": 1948,
                "title": "A Better You",
                "author": "James Blake",
                "genre": [
                    "self-help"
                ]
            };

            await db.collection(LIBRARY_COLLECTION).insertOne(libraryDetails);
            const returnedBook = await resolvers.Mutation.createBook(null, { input: { branchId: libraryId, genre: bookDetails.genre, year: bookDetails.year, title: bookDetails.title, author: { name: bookDetails.author } } }, context);
            const storedLibrary = await db.collection(LIBRARY_COLLECTION).findOne({ _id: ObjectId(libraryId) });
            const expectedBook = storedLibrary.books[0];
            delete expectedBook.id;
            expectedBook[branchId] = libraryId;
            delete returnedBook.id;

            expect(returnedBook).toEqual(expectedBook);

        });
        it('if exception is thrown method throws', async () => {
            await expect(resolvers.Mutation.createBook(null, null, context)).rejects.toThrow();
        });


    });

    describe('createLibrary', () => {

        let connection;
        let db;
        let context;

        beforeAll(async () => {
            connection = await MongoClient.connect(process.env.MONGO_URL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            db = await connection.db();
            
            const collections = await db.collections();
            
            if(collections.some(collection => collection.collectionName === LIBRARY_COLLECTION)){
                db.collection(LIBRARY_COLLECTION).drop();
            }

            context = {
                dbClient: db
            }
        });

        afterAll(async () => {
            await connection.close();
        });

        it('method throws if response is not okay', async () => {

            const response = {
                result: {
                    ok: 0
                }
            };

            const libraryCollection = {
                insertOne: (document) => {
                    console.log(document);
                    return response;
                }
            };

            const localContext = {
                dbClient: {
                    collection: (_) =>  libraryCollection
                    
                }
            };

            await expect(resolvers.Mutation.createLibrary(null, {branch: 'Any valid branch name'}, localContext)).rejects.toThrow('Error creating new library');

        });

        it('method inserts library in db', async () => {

            const branchName = 'Test Library';

            await resolvers.Mutation.createLibrary(null, {branch: branchName}, context);
            const storedLibrary = await db.collection(LIBRARY_COLLECTION).findOne({ branch: branchName });
            
            expect(storedLibrary).toBeDefined();

        });

        it('returned library matches stored library', async () => {

            const branchName = 'Testing Returned Library Is Stored';
            const expectedLibrary = {
                "branch": branchName,
                "books": []
            };

            const returnedLibrary = await resolvers.Mutation.createLibrary(null, {branch: expectedLibrary.branch}, context);
            delete returnedLibrary.id

            expect(returnedLibrary).toEqual(expectedLibrary);

        });

        it('if exception is thrown method throws', async () => { 
            await expect(resolvers.Mutation.createLibrary(null, {branch: null}, null)).rejects.toThrow();
        });


    });

    describe('deleteLibrary', () => {

        let connection;
        let db;
        let context;

        beforeAll(async () => {
            ({ connection, db, context } =  await setUpDatabase());
        });

        afterAll(async () => {
            await connection.close();
        });

        it('method throws if response deleted count is 0', async () => {

            const response = {
                deletedCount: 0
            };

            const libraryCollection = {
                deleteOne: ({_}) => {
                    return response;
                }
            };

            const localContext = {
                dbClient: {
                    collection: (_) =>  libraryCollection
                    
                }
            };

            const localBranchId = '604b08d8313ea78820db5b67'

            await expect(resolvers.Mutation.deleteLibrary(null, {branchId: localBranchId}, localContext)).rejects.toThrow(`Error deleting library with Id ${localBranchId}`);

        });

        it('method throws if response deleted count is undefined', async () => {

            const response = {
            };

            const libraryCollection = {
                deleteOne: ({_}) => {
                    return response;
                }
            };

            const localContext = {
                dbClient: {
                    collection: (_) =>  libraryCollection
                    
                }
            };

            const localBranchId = '604b08d8313ea78820db5b67'

            await expect(resolvers.Mutation.deleteLibrary(null, {branchId: localBranchId}, localContext)).rejects.toThrow(`Error deleting library with Id ${localBranchId}`);

        });

        it('method deletes library in db', async () => {

            const libraryId = '604b08d8313ea78820db5b18';
            const libraryDetails = {
                _id: ObjectId(libraryId),
                "branch": "New Kingston",
                "books": []
            };

            await db.collection(LIBRARY_COLLECTION).insertOne(libraryDetails);
            await resolvers.Mutation.deleteLibrary(null, {branchId: libraryId}, context);
            const storedLibrary = await db.collection(LIBRARY_COLLECTION).findOne({ _id: ObjectId(libraryId) });
            
            expect(storedLibrary).toBeNull();

        });

        it('if exception is thrown method throws', async () => { 
            await expect(resolvers.Mutation.deleteLibrary(null, {branchId: null}, null)).rejects.toThrow();
        });


    });

    describe('deleteBook', () => {

        let connection;
        let db;
        let context;
        const bookId = '604b08d8313ea78820db3b45';

        beforeAll(async () => {
            ({connection, db, context} = await setUpDatabase())
        });

        afterAll(async () => {
            await connection.close();
        });

        it('method throws if response number of modified count is 0', async () => {

            const response = {
                result: {
                    nModified: 0
                }
            };

            const libraryCollection = {
                updateOne: ({_}) => {
                    return response;
                }
            };

            const localContext = {
                dbClient: {
                    collection: (_) =>  libraryCollection
                    
                }
            };

            const localBranchId = '604b08d8313ea78820db5b67'

            await expect(resolvers.Mutation.deleteBook(null, {branchId: localBranchId, bookId: bookId}, localContext)).rejects.toThrow(`Error deleting book with Id ${bookId}`);

        });

        it('method deletes library book in db', async () => {

            const libraryId = '604b08d8313ea78820db5b09';

            const bookDetails = {
                "id": ObjectId(bookId),
                "year": 1948,
                "title": "A Better You",
                "author": "James Blake",
                "genre": [
                    "self-help"
                ]
            };

            const libraryDetails = {
                _id: ObjectId(libraryId),
                "branch": "New Kingston",
                "books": [
                    bookDetails
                ]
            };

            await db.collection(LIBRARY_COLLECTION).insertOne(libraryDetails);
            await resolvers.Mutation.deleteBook(null, {bookId: bookId, branchId: libraryId}, context);

            const storedLibrary = await db.collection(LIBRARY_COLLECTION).findOne({ _id: ObjectId(libraryId) });
            
            expect(storedLibrary.books.length).toBe(0);

        });

        it('if exception is thrown method throws', async () => { 
            await expect(resolvers.Mutation.deleteBook(null, {branchId: null, bookId: null}, null)).rejects.toThrow();
        });


    });

});

describe('Books', () => {
    describe('author', ()=> {
        it('method returns author object', () => {
            const book = {
                "id": ObjectId("604b0afb313ea78820db5b39"),
                "year": 1994,
                "title": "Through The Rain",
                "author": "Leslie Williams",
                "genre": [
                    "comedy"
                ]
            };

            const expectedAuthor = {
                name: book.author
            }

            const returnedAuthor = resolvers.Book.author(book);

            expect(returnedAuthor).toEqual(expectedAuthor);
        });
    });
});

