import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import typeDefs from './schema';
import resolvers  from './resolvers';
import { MongoClient } from 'mongodb';

const server = new ApolloServer({typeDefs, resolvers, context: async () => ({
    dbClient: (await MongoClient.connect('mongodb://127.0.0.1:27017', { useUnifiedTopology: true })).db('graphql')
})});
const app = express();
server.applyMiddleware({app});


app.listen(4000, () => console.log('Running at port 4000' + server.graphqlPath));